package assignment6;


/**
 * seat objects for a theater that can be marked available or not, and contain priority
 * implements comparable by comparing only priority
 * @author Mitch Hinrichs and Andrew Surratt
 * @version 1.0
 */
public class Seat implements Comparable<Seat>
{
	private int priority; 
	private String seatSection;
	private String seatRow;
	private String seatNum;
	
	public Seat()
	{
		priority = 0;
		seatSection = null;
		seatRow = null;
		seatNum = null;
	}
	/**
	 * Seat constructor
	 * @param section middle, house left, house right
	 * @param row A, B, C... through AA
	 * @param num seat number between 101 and 128 (114 and 115 in middle)
	 * @param newPriority higher priority is better
	 */
	public Seat(String section, String row, String num, int newPriority)
	{
		this.priority = newPriority;
		this.seatSection = section;
		this.seatRow = row;
		this.seatNum = num;
	}
	
	/**
	 * Seat copy constuctor
	 * seat the seat object to copy
	 */
	public Seat(Seat seat)
	{
		this(seat.getSection(), seat.getRow(), seat.getNum(), seat.getPriority());
	}
	
	/**
	 * compareTo specifies how to compare seat objects
	 * @param otherSeat seat to compare to this seat
	 * @return how much higher priority otherSeat is than this seat
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Seat otherSeat)
	{
		return (this.priority - otherSeat.priority);
	}
	
	/**
	 * getSection gets the seat section name (MIDDLE, HOUSE LEFT, or HOUSE RIGHT)
	 * @return section name
	 */
	public String getSection()
	{return this.seatSection;}
	
	/**
	 * getRow gets the seat row letter (A through AA)
	 * @return row letter
	 */
	public String getRow()
	{return this.seatRow;}
	
	/**
	 * getNum gets the seat number (101 to 128)
	 * @return the seat number
	 */
	public String getNum()
	{return this.seatNum;}
	
	/**
	 * getPriority gets the priority of the seat (the lower numbers represent higher priorities)
	 * @return seat priority
	 */
	public int getPriority()
	{return this.priority;}
}