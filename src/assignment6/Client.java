package assignment6;

/**
 * Client class creates clients that are represented by a number
 * implements the comparable interface to sort clients by number
 * @author Mitch Hinrichs and Andrew Surratt
 * @version 1.0
 */
public class Client implements Comparable<Client>
{
	int clientNum;
	
	public Client()
	{
		clientNum = -1;
	}
	
	public Client(int newCustomerNum)
	{
		this.clientNum = newCustomerNum;
	}
	
	/**
	 * compareTo specifies how to compare client objects
	 * @param otherClient seat to compare to this client
	 * @return how much higher priority otherClient is than this client
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Client otherClient)
	{
		return (this.clientNum - otherClient.clientNum);
	}
	
	/**
	 * getNum gets the number representing the client
	 * @return the integer representing the client
	 */
	public int getNum()
	{
		return this.clientNum;
	}
}
