package assignment6;

import java.util.concurrent.locks.ReentrantLock;

/**
 * creates a box office for a given theater
 * multi-threadable
 * @author Mitch Hinrichs and Andrew Surratt
 * @version 1.0
 */
public class BoxOffice implements Runnable
{
	String boxNum;
	Theater theater;
	ReentrantLock myLock = new ReentrantLock();
	int numberOfClients;
	
	
	public BoxOffice()
	{
		this.boxNum = null;
		theater = null;
	}
	
	public BoxOffice(String newBoxNum, Theater newTheater)
	{
		this.boxNum = newBoxNum;
		theater = newTheater;
	}
	
	/**
	 * run threaded method that will check for best seats, mark them as available, and print tickets
	 */
	public void run()
	{		
		//while seats are still available, sell seats
		while(theater.getNumSeats() > 0)
		{
			Seat bestSeat = this.theater.bestAvailableSeat(); // finds and polls best seat
			int nextClient = theater.getClient().getNum();
			//System.out.println("Box office " + boxNum + " is selling client " + nextClient + " a ticket: " + this.theater.getSeatInfo(bestSeat));
			
			//print seats
			if(bestSeat != null)
			{
				this.printTicketSeat(bestSeat, nextClient);
			}
			else
			{
				System.out.println("I'm sorry but we are sold out."); // never executed from what I can tell but it's in the requirements
			}
			
		}
		System.out.println("I'm sorry but box " + boxNum + " is sold out.");
	}
	
	/**
	 * prints to the console the ticket info for a given seat
	 * @param seat the seat to print info for
	 */
	public void printTicketSeat(Seat seat, int nextClient)
	{
		System.out.println("Box office " + boxNum + " sold client number " + nextClient + " a ticket: " + this.theater.getSeatInfo(seat));
	}
}