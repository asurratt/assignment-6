package assignment6;


/**
 * Assignment 6
 * handles seat sales for a theater
 * EE422C Monday 3-4:30
 * as52224 and mfh537
 * @author Mitch Hinrichs and Andrew Surratt
 * @version 1.0
 * @see Seat
 * @see Theater
 * @see BoxOffice
 * @see Client
 */

public class A6Driver
{	
	public static void main(String[] args)
	{
		Theater bates = new Theater();
		BoxOffice boxA = new BoxOffice("A", bates);
		BoxOffice boxB = new BoxOffice("B", bates);
		//System.out.println(bates.bestAvailableSeat().priority);
		Thread tobj1;
		Thread tobj2;
		// executes one thread for each box office
		tobj1 = new Thread(boxA);
		tobj2 = new Thread (boxB);
		tobj1.start(); 
		tobj2.start();
	}

}
