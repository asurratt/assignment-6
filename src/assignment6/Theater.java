package assignment6;

//import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * creates a theater modeled after bates concert hall
 * has methods for checking and getting best seats
 * @author Mitch Hinrichs and Andrew Surratt
 * @version 1.0
 */
public class Theater
{
	private int totalSeatsAvailable;
	private PriorityQueue<Seat> seatQ;
	ReentrantLock theLock = new ReentrantLock();
	private PriorityQueue<Client> clientQ;
	private int numClients;
	private final String[] ROWS = {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	
	public Theater()
	{
		totalSeatsAvailable = 0;
		
		
		// make client queue
		clientQ = new PriorityQueue<Client>();
		Random randNum = new Random();
		int randInt = randNum.nextInt(901); // between 0 and 900
		randInt += 100; // between 100 and 1000
		numClients = randInt;
		for(int x = 1; x <= numClients; x++)
		{
			Client temp = new Client(x);
			clientQ.add(temp);
		}
		
		/*Iterator<Client> it = clientQ.iterator();
		while(it.hasNext())
		{
			Client temp = it.next();
			System.out.println("client num: "+ temp.customerNum);
		}*/
		
		// assign seats
		seatQ = new PriorityQueue<Seat>();
		Seat addSeat;
		int priorityMid = 1;
		int prioritySide = 2;
		
		for(String row: ROWS)
		{
			for(int x = 108; x <= 121; x++)
			{
				Integer num = (Integer) x;
				addSeat = new Seat("MIDDLE", row, num.toString(), priorityMid);
				seatQ.add(addSeat);
				totalSeatsAvailable++;
			}
			priorityMid += 2;
			if(row != "A")
			{
				for(int x = 101; x <= 107; x++)
				{
					Integer num = (Integer) x;
					addSeat = new Seat("HOUSE LEFT", row, num.toString(), prioritySide);
					seatQ.add(addSeat);
					totalSeatsAvailable++;
				}
				for(int x = 122; x <= 128; x++)
				{
					Integer num = (Integer) x;
					addSeat = new Seat("HOUSE RIGHT", row, num.toString(), prioritySide);
					seatQ.add(addSeat);
					totalSeatsAvailable++;
				}
			}
			prioritySide += 2;
		}
		for(int x = 111; x <= 118; x++)
		{
			Integer num = (Integer) x;
			addSeat = new Seat("MIDDLE", "AA", num.toString(), 49);
			seatQ.add(addSeat);
			totalSeatsAvailable++;
		}
		for(int x = 104; x <= 107; x++)
		{
			Integer numLeft = (Integer) x;
			addSeat = new Seat("HOUSE LEFT", "A", numLeft.toString(), 2);
			seatQ.add(addSeat);
			totalSeatsAvailable++;
			
			Integer numRight = (Integer) (x + 18);
			addSeat = new Seat("HOUSE RIGHT", "A", numRight.toString(), 2);
			seatQ.add(addSeat);
			totalSeatsAvailable++;
		}
		for(int x = 101; x <= 104; x++)
		{
			Integer numLeft = (Integer) x;
			addSeat = new Seat("HOUSE LEFT", "AA", numLeft.toString(), 50);
			seatQ.add(addSeat);
			totalSeatsAvailable++;
			
			Integer numRight = (Integer) (x + 24);
			addSeat = new Seat("HOUSE RIGHT", "AA", numRight.toString(), 50);
			seatQ.add(addSeat);
			totalSeatsAvailable++;
		}
		
		/*print FULL seat map
		Iterator<Seat> it = seatQ.iterator();
		while(it.hasNext())
		{
			Seat temp = it.next();
			System.out.println(getSeatInfo(temp));
		}*/
		//System.out.println("number of seats: " + totalSeatsAvailable);
	}
	
	/**
	 * finds best available seat, updates total seats left in the theater, and polls the next seat
	 * @return best available seat or null if no seat is available
	 */
	public synchronized Seat bestAvailableSeat()
	{
		Seat bestSeat;

		bestSeat = seatQ.poll();
		if(bestSeat != null)
		{
			markAvailableSeatTaken(bestSeat);
		}
		else
		{
			return null;
		}
		return bestSeat;
		
	}
	
	/**
	 * updates the number of seats left in the theater
	 * @param takeSeat is the seat you want to take
	 * @return seat if it was successfully taken, otherwise returns null
	 */
	public Seat markAvailableSeatTaken(Seat takeSeat)
	{
		totalSeatsAvailable -= 1;
		return takeSeat;

	}
	/**
	 * gets seat's section row and number
	 * @param seat the seat to get info of
	 * @return String of seat info
	 */
	public String getSeatInfo(Seat seat)
	{
		StringBuilder seatInfoBuild = new StringBuilder();
		seatInfoBuild.append(seat.getSection());
		seatInfoBuild.append(" ");
		seatInfoBuild.append(seat.getRow());
		seatInfoBuild.append(" ");
		seatInfoBuild.append(seat.getNum());
		//seatInfoBuild.append(" ");
		//seatInfoBuild.append(seat.getPriority());
		
		String seatInfo = seatInfoBuild.toString();
		//System.out.println(seatInfo);
		return seatInfo;
	}
	
	/**
	 * finds number of seats available
	 * @return number of seats available
	 */
	public int getNumSeats()
	{
		return this.totalSeatsAvailable;
	}
	
	/**
	 * getClient polls the next 
	 * @return
	 */
	public synchronized Client getClient()
	{
		numClients += 1;
		clientQ.add(new Client(numClients));
		return clientQ.poll();
	}
}